package ru.cd.himawari;

import android.os.Bundle;
import android.preference.PreferenceActivity;

/**
 * User: Vlad
 * Date: 07.02.2016
 * Time: 0:43
 */
public class MySettingsActivity extends PreferenceActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.prefs);
//        setContentView(R.layout.layout);
//        final SeekBar scalerator = (SeekBar) findViewById(R.id.scalerator);
//        final CompoundButton only_wifi_button = (CompoundButton) findViewById(R.id.only_wifi_button);
//        final TextView scale_text = (TextView) findViewById(R.id.scale_text);
//
////        scalerator.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
////            @Override
////            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
////                float scale = progress / 10f;
////                scale_text.setText(String.format("%sx", scale));
////                PreferenceManager.getDefaultSharedPreferences(MySettingsActivity.this).edit().putFloat("scale", scale).commit();
////            }
////
////            @Override
////            public void onStartTrackingTouch(SeekBar seekBar) {
////            }
////
////            @Override
////            public void onStopTrackingTouch(SeekBar seekBar) {
////            }
////        });
//
//        only_wifi_button.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                PreferenceManager.getDefaultSharedPreferences(MySettingsActivity.this).edit().putBoolean("only_wifi", isChecked).commit();
//            }
//        });
    }
}
