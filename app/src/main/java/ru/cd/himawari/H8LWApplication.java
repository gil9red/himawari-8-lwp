package ru.cd.himawari;

import android.app.Application;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

/**
 * User: Vlad
 * Date: 07.02.2016
 * Time: 17:12
 */
public class H8LWApplication extends Application {
    private Tracker mTracker;

    @Override
    public void onCreate() {
        super.onCreate();
        Tracker defaultTracker = getDefaultTracker();
        defaultTracker.setScreenName("Hello World!");
        defaultTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    synchronized public Tracker getDefaultTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            mTracker = analytics.newTracker(R.xml.global_tracker);
            mTracker.enableAdvertisingIdCollection(true);
        }
        return mTracker;
    }
}
