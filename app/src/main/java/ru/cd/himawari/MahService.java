package ru.cd.himawari;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.service.wallpaper.WallpaperService;
import android.util.Log;
import android.util.TypedValue;
import android.view.SurfaceHolder;

import org.joda.time.DateTime;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * User: Vlad
 * Date: 06.02.2016
 * Time: 16:55
 */
public class MahService extends WallpaperService {
    private final static String TAG = "HIMAWARI";
    private final String filename = "himawari.jpg";

    @Override
    public Engine onCreateEngine() {
        return new MyWallpaperEngine();
    }

    private class MyWallpaperEngine extends Engine {
        private final Handler handler = new Handler();
        private final Runnable loadRunner = new Runnable() {
            @Override
            public void run() {
                Thread d = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        loadPictures();
                    }
                });
                d.start();
            }
        };
        private Paint paint = new Paint();
        private int canvas_width;
        private int canvas_height;
        private final static int level = 4;
        private final static int width = 550;
        private Bitmap[][] bitmaps = new Bitmap[level][level];

        public MyWallpaperEngine() {
            paint.setAntiAlias(true);
            handler.post(loadRunner);

            PreferenceManager.getDefaultSharedPreferences(MahService.this).registerOnSharedPreferenceChangeListener(new SharedPreferences.OnSharedPreferenceChangeListener() {
                @Override
                public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
                    if (key.equals("scale")) {
                        draw();
                    }
                }
            });
        }

        @Override
        public void onSurfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            this.canvas_width = width;
            this.canvas_height = height;
            super.onSurfaceChanged(holder, format, width, height);
            draw();
        }


        private void draw() {
            float scale = 1f;//PreferenceManager.getDefaultSharedPreferences(MahService.this).getFloat("scale", 1f);
            SurfaceHolder holder = getSurfaceHolder();
            Canvas canvas = null;
            try {
                canvas = holder.lockCanvas();
                if (canvas != null) {
                    paint.setTextAlign(Paint.Align.CENTER);
                    paint.setColor(Color.WHITE);
                    paint.setTextSize(dp(26));
                    canvas.drawText("Loading///", canvas_width / 2, canvas_width / 2, paint);

                    int size = (int) (Math.max(canvas_height, canvas_width) * scale);

                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                    Bitmap bitmap = BitmapFactory.decodeStream(openFileInput(filename), null, options);
                    if (bitmap != null)
                        canvas.drawBitmap(getResizedBitmap(bitmap, size, size), canvas_width / 2 - size / 2, canvas_width / 2 - size / 2, paint);
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } finally {
                if (canvas != null)
                    holder.unlockCanvasAndPost(canvas);
            }
        }

        private float dp(int i) {
            Resources r = getResources();
            return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, i, r.getDisplayMetrics());
        }

        public Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {
            int width = bm.getWidth();
            int height = bm.getHeight();
            float scaleWidth = ((float) newWidth) / width;
            float scaleHeight = ((float) newHeight) / height;
            // create a matrix for the manipulation
            Matrix matrix = new Matrix();
            // resize the bit map
            matrix.postScale(scaleWidth, scaleHeight);
            // recreate the new Bitmap
            return Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
        }

        private void loadPictures() {
            boolean only_wifi = PreferenceManager.getDefaultSharedPreferences(MahService.this).getBoolean("only_wifi", false);
            if (!only_wifi) {
                ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

                if (!mWifi.isConnected()) {
                    handler.removeCallbacks(loadRunner);
                    handler.postDelayed(loadRunner, 1000 * 10 * 60);
                    return;
                }
            }

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd/HHmmss", Locale.getDefault());
            Date date = new Date();
            DateTime dt = new DateTime(date);
            dt = dt.plusMillis(-dt.getZone().getOffset(dt));
            dt = dt.plusSeconds(-dt.getSecondOfMinute());
            dt = dt.plusMinutes(-30 - dt.getMinuteOfHour() % 10);

            try {
                Log.d(TAG, "Need to load:");
                for (int y = 0; y < level; y++) {
                    for (int x = 0; x < level; x++) {
                        Date date1 = dt.toDate();
                        String url = String.format(
                                Locale.getDefault(),
                                "http://himawari8-dl.nict.go.jp/himawari8/img/D531106/%dd/%d/%s_%d_%d.png",
                                level,
                                width,
                                simpleDateFormat.format(date1),
                                x,
                                y);
                        Log.d(TAG, url);
                        URL url2 = new URL(url);
                        Bitmap bmp = BitmapFactory.decodeStream(url2.openConnection().getInputStream());
                        bitmaps[y][x] = bmp;
                    }
                }
                Log.d(TAG, "Load complete!");
            } catch (IOException e) {
                e.printStackTrace();
            }

            Bitmap bitmap = Bitmap.createBitmap(width * level, width * level, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);

            for (int y = 0; y < level; y++) {
                for (int x = 0; x < level; x++) {
                    canvas.drawBitmap(
                            bitmaps[y][x],
                            x * width,
                            y * width,
                            paint);
                }
            }


            try {
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, openFileOutput(filename, Context.MODE_PRIVATE));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            draw();
            handler.removeCallbacks(loadRunner);
            handler.postDelayed(loadRunner, 1000 * 10 * 60);
        }
    }
}
